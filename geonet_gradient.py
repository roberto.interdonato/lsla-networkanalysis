import networkx as nx
#from mpl_toolkits.basemap import Basemap as Basemap
import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.io.shapereader as shpreader
import numpy as np
from colour import Color
import matplotlib as mpl
from descartes import PolygonPatch
from scalebar import north_arrow
import os

dataset_name = 'global'
#dataset_name = "agriculture+biofuel"
#dataset_name = "global"

#os.chdir("D:\\Mes Donnees\\Land Matrix\\mines_PNAS\\")

os.chdir("/Users/interdonato/Documents/Land Matrix/Exports/27.02.2023.Mines/nets/")

degrees = ['in', 'out','inout','inout_num']

degree = degrees[3]

path_iso = "ISO country.xlsx"

#path_d = "ids_to_countries_mines_mlnet.csv"
#path = "mines_mlnet_flattened.ncol"

#path_d = "ids_to_countries_mines_APR21.csv"
#path_d = "ids_to_countries_mines_mlnet.csv"
path_d ="ids_to_countries_mines_27022023_inOperation.csv"
#path = "mines_mlnet_flattened.ncol"
path = "net_mines_27022023_inOperation.ncol"

#path = ".\\data_net\\net_%s_surf_inOperation.ncol" % dataset_name

#path_d = ".\\data_net\\ids_to_countries_%s_inOperation.csv" % dataset_name

graph = nx.read_edgelist(path, delimiter=';', create_using=nx.DiGraph(), nodetype=int, data=(('weight', int),))



names_dict = {}
coord = {}
lat = []
long = []


fc = open("countries_to_coord_allTransnational.csv", 'r',encoding="latin-1")
for line in fc:
    vals=line.split(';')
    coord[vals[0]] = (float(vals[1]), float(vals[2].strip()))

inv_names_dict = {}

fd = open(path_d,'r')
for line in fd:
    vals=line.split(';')
    country = vals[1].strip()
    names_dict[int(vals[0])]=country
    inv_names_dict[country]=int(vals[0])
    #location = geolocator.geocode(vals[1])
    lat.append(coord[country][0])
    long.append(coord[country][1])

    #fc.write("%s;%s;%s\n"   %  (vals[1].strip(),str(coord[vals[1]][0]),str(coord[vals[1]][1])))
print("Names and coordinates successfully loaded.")

fc.close()







#red = Color("red")
#colors = list(red.range_to(Color("green"),len(names_dict.keys())))


#plt.figure(figsize=(20,18))

"""" 
#m = Basemap(projection='merc', llcrnrlon=-180, llcrnrlat=10, urcrnrlon=-50,urcrnrlat=70, lat_ts=0, resolution='l', suppress_ticks=True)
#m = Basemap(projection='merc', lon_0=0,resolution='l')
m = Basemap(projection='merc', llcrnrlon=-180, llcrnrlat=-60, urcrnrlon=180, urcrnrlat=70)

# Control the background color
m.drawmapboundary(fill_color='#A6CAE0', linewidth=0)
# Fill the continent
m.fillcontinents(color='grey', alpha=0.5, lake_color='grey')
# Draw the coastline
m.drawcoastlines(linewidth=0.2)
m.drawstates(linewidth=0.1)
m.drawcountries(linewidth=0.1)

mx, my = m(long, lat)
"""

pos = {}
for id in names_dict:
        pos[int(id)]=(coord[names_dict[id]][1],coord[names_dict[id]][0])



iso =  pd.read_excel(path_iso, index_col=0)

iso_dict = {}
inv_iso_dict= {}
for id in names_dict:
    iso_dict[id]=iso.loc[names_dict[id]]['Code ISO']
    inv_iso_dict[iso.loc[names_dict[id]]['Code ISO']]=id

""""
foso = open("ids_isos.csv",'w')
for id in iso_dict:
    foso.write("%d;%s\n" % (id,iso_dict[id]))
foso.close()
exit()
"""

size_deg_dict=None

if degree=='in':
    size_deg_dict = dict(graph.in_degree(weight='weight'))
elif degree=='out':
    size_deg_dict = dict(graph.out_degree(weight='weight'))
elif degree=='inout':
    size_deg_dict = dict(graph.in_degree(weight='weight'))
    outdict = dict(graph.out_degree(weight='weight'))
    for k in size_deg_dict:
        size_deg_dict[k]=(size_deg_dict[k]+1)/(outdict[k]+1)
elif degree=="inout_num":
    size_deg_dict = dict(graph.in_degree(weight='weight'))
    outdict = dict(graph.out_degree(weight='weight'))
    for k in size_deg_dict:
        size_deg_dict[k]=(size_deg_dict[k]+1)/(outdict[k]+1)
    """
    size_deg_dict = dict(graph.in_degree())
    outdict = dict(graph.out_degree())
    for k in size_deg_dict:
        size_deg_dict[k] = (size_deg_dict[k] + 1) / (outdict[k] + 1)
    """

#thres = np.percentile(list(size_deg_dict.values()),90)
#print("Percentile 90:",thres)
#labels = {}
#for id in iso_dict:
#    if size_deg_dict[id]>=thres:
#        labels[id]=iso_dict[id]
#    else:
#        labels[id]=''


# Map projection
fig = plt.figure(figsize=(19.20,10.80))

crs = ccrs.PlateCarree(central_longitude=0.0, globe=None)
#fig, ax = plt.subplots(1, 1, figsize=(12, 8), subplot_kw=dict(projection=crs))
ax = plt.axes(projection=crs)
ax.coastlines()
#ax.set_extent([-90, -180, 90, 180])
ax.set_global()


#scale_bar(ax, (0.4, 0.2), 6_000)


#backround image
#si = ax.stock_img()
#si.set_zorder(-1)

#north_arrow(ax, (0.1,0.2), 500)


"""" 
nx.draw_networkx_nodes(G=graph, pos=pos, node_list=size_deg_dict.keys(), node_color='r', alpha=0.8, node_size=[v/1000  for v in size_deg_dict.values()])
nx.draw_networkx_edges(G=graph, pos=pos, edge_color='g', alpha=0.2, arrows=True)
nx.draw_networkx_labels(G=graph, pos=pos, labels=iso_dict)
"""

#plt.tight_layout()
#plt.savefig("map_in_allTransnational_wSurface.png", format="png", dpi=300)
#plt.savefig("map_%s_allTransnational_wSurface_isoCodes_inOperation.png" % degree, format="png", dpi=300)

#plt.savefig("map_%s_%s.png" % (degree,dataset_name), format="png", bbox_inches='tight',dpi=300)

shapename = 'admin_0_countries'
countries_shp = shpreader.natural_earth(resolution='110m',
                                        category='cultural', name=shapename)

#odict = OrderedDict(size_deg_dict)

# Inutile: la norm di python li mette cmq a 0
min_d = np.min(list(size_deg_dict.values()))
for x in size_deg_dict:
    if size_deg_dict[x]==0:
        size_deg_dict[x]=min_d

score={}
for x in size_deg_dict:
    score[x]=np.log10(size_deg_dict[x])

""""

print("Printing dicts")

fom = open("Mscore_%s.csv"  % dataset_name,'w')
for n in score:
    fom.write("%s;%f\n" % (iso_dict[n],score[n]))
fom.close()

in_deg_dict = dict(graph.in_degree(weight='weight'))


out_deg_dict = dict(graph.out_degree(weight='weight'))

size_deg_dict


fom = open("SizesInOut_%s.csv" % dataset_name,'w')
for n in size_deg_dict:
    fom.write("%s;%f\n" % (names_dict[n],size_deg_dict[n]))
fom.close()

exit()
"""


""""
fom = open("LMscore_%s.csv"  % dataset_name,'w')
for n in score:
    fom.write("%s;%f\n" % (iso_dict[n],score[n]))
fom.close()
"""
in_dict = dict(graph.in_degree())
out_dict = dict(graph.out_degree())


fom = open("SizesIn_%s.csv" % dataset_name,'w')
for n in size_deg_dict:
    fom.write("%s;%f\n" % (iso_dict[n],in_dict[n]))
fom.close()


fom = open("SizesOut_%s.csv" % dataset_name,'w')
for n in size_deg_dict:
    fom.write("%s;%f\n" % (iso_dict[n],out_dict[n]))
fom.close()



#cmap = mpl.cm.RdYlGn
#cmap = mpl.cm.jet
cmap = mpl.cm.gist_rainbow

#norm = plt.Normalize(np.min(list(size_deg_dict.values())), np.max(list(size_deg_dict.values())))
norm = plt.Normalize(np.min(list(score.values())), np.max(list(score.values())))

#color = cmap(norm(df.loc[state.attributes['name'], 'value']))


patches=[]
for country in shpreader.Reader(countries_shp).records():
    patches.append(PolygonPatch(country.geometry))
    if country.attributes['WB_A3'] in inv_iso_dict:
        #isos[country.attributes['WB_A3']]=country.attributes['NAME_LONG']
        #idx = list(odict.keys()).index(inv_iso_dict[country.attributes['WB_A3']])
        curr_id = inv_iso_dict[country.attributes['WB_A3']]
        if curr_id in score:
            ax.add_geometries([country.geometry], ccrs.PlateCarree(), facecolor=cmap(norm(score[inv_iso_dict[country.attributes['WB_A3']]])), label=country.attributes['NAME_LONG'],zorder=0)
    elif country.attributes['ISO_A3_EH'] in inv_iso_dict:
        #isos[country.attributes['ISO_A3_EH']]=country.attributes['NAME_LONG']
        ax.add_geometries([country.geometry], ccrs.PlateCarree(), facecolor=cmap(norm(score[inv_iso_dict[country.attributes['ISO_A3_EH']]])), label=country.attributes['NAME_LONG'],zorder=0)
        #match+=1
    elif country.attributes['ISO_A3'] in inv_iso_dict:
        #isos[country.attributes['ISO_A3']]=country.attributes['NAME_LONG']
        ax.add_geometries([country.geometry], ccrs.PlateCarree(), facecolor=cmap(norm(score[inv_iso_dict[country.attributes['ISO_A3']]])), label=country.attributes['NAME_LONG'],zorder=0)
        #match+=1
    elif country.attributes['NAME_LONG'] in inv_names_dict:
        #isos[country.attributes['ISO_A3']]=country.attributes['NAME_LONG']
        ax.add_geometries([country.geometry], ccrs.PlateCarree(), facecolor=cmap(norm(score[inv_names_dict[country.attributes['NAME_LONG']]])), label=country.attributes['NAME_LONG'],zorder=0)
        #match+=1
    else:
        #ax.add_geometries([country.geometry], ccrs.PlateCarree(), facecolor=colors[int(len(colors)/2)].rgb, label=country.attributes['NAME_LONG'])
        ax.add_geometries([country.geometry], ccrs.PlateCarree(), facecolor=Color("lightgrey").rgb,label=country.attributes['NAME_LONG'],zorder=0)

        #print("Missing country:",country.attributes['ISO_A3'],country.attributes['NAME_LONG'],country.attributes['ISO_A2'],country.attributes['ISO_A3_EH'],country.attributes['WB_A3'])


"""
#print(match,len(odict))
print("====================================================================")
for x in isos:
    print(x,';',isos[x])
print("====================================================================")
for x in inv_iso_dict.keys():
    print(x,';',names_dict[inv_iso_dict[x]])
print("====================================================================")

fom = open('matches.csv','w')
for i in inv_iso_dict:
    if i in isos:
        fom.write("%s;%s;%s;%s\n"  %   (i,isos[i],names_dict[inv_iso_dict[i]],"TRUE"))
    else:
        fom.write("%s;-;%s;%s\n" % (i,names_dict[inv_iso_dict[i]],"FALSE"))

fom.close()
"""



edge_size_dict = [graph[u][v]['weight']*10 for u, v in graph.edges]
wmax = 0
wmin = np.max(edge_size_dict)
for u, v in graph.edges:
    if graph[u][v]['weight'] > wmax:
        wmax = graph[u][v]['weight']
    if graph[u][v]['weight'] < wmin:
        wmin =graph[u][v]['weight']


nrange = 0.8
low = 0.2
edge_alphas=[((graph[u][v]['weight']-wmin)*nrange)/(wmax-wmin)+low for u,v in graph.edges]





#arrowstyle=ArrowStyle.CurveFilledB(head_length=1, head_width=1)
edges_d = nx.draw_networkx_edges(G=graph,ax=ax, pos=pos, width=1,edge_color ="darkgreen", alpha=0.2, style="solid", arrows=True, connectionstyle='arc3,rad=0.2')
#edges_d = nx.draw_networkx_edges(G=graph,ax=ax, pos=pos, width=0.2,edge_color =edge_size_dict, alpha=0.2, arrows=True, connectionstyle='arc3,rad=0.2',  edge_cmap=plt.cm.Wistia)

#nodes_d = nx.draw_networkx_nodes(G=graph,ax=ax, pos=pos, node_list=size_deg_dict.keys(), node_color='b', alpha=0.5,                      node_size=10).set_zorder(2)
#edges_d = nx.draw_networkx_edges(G=graph,ax=ax, pos=pos, width=0.2,edge_color='blue', alpha=0.2, arrows=True, connectionstyle='arc3,rad=0.2')
#labs = nx.draw_networkx_labels(G=graph,ax=ax, pos=pos, labels=labels,alpha=0.8)
#plt.show()

M = graph.number_of_edges()


for i in range(M):
    val = edge_alphas[i]+0.04
    if val>1.0:
        val=1.0
    edges_d[i].set_alpha(val)
    #if edge_alphas[i] < 0.5:
    #   edges_d[i].set_alpha(edge_alphas[i] + 0.3)
    #else:
    #    edges_d[i].set_alpha(edge_alphas[i])


pc = mpl.collections.PatchCollection(patches, cmap=cmap)
pc.set_array(norm(list(score.values())))


#divider = make_axes_locatable(ax)
#cax = divider.append_axes("right", size="5%", pad=0.05)

#plt.colorbar(pc,fraction=0.046, pad=0.05)
cb = plt.colorbar(pc,fraction=0.023, pad=0.05)
cb.ax.tick_params(labelsize=20)


#ax.set_axis_off()
plt.tight_layout()
#nodes_d.set_zorder(20)
#edges_d.set_zorder(20)
"""
nx.draw_networkx(graph, ax=ax,
                 font_size=16,
                 alpha=.3,
                 width=0.5,
                 edge_color="grey",
                 #edge_color="darkblue",
                 #node_size=[v/1000  for v in size_deg_dict.values()],
                 node_size=10,
                 labels=labels,
                 pos=pos,
                 #node_color=altitude,
                 arrows=True,
                 connectionstyle='arc3,rad=0.2',
                 cmap=plt.cm.autumn)
"""

plt.savefig("map_mines_2023_good.pdf", format="pdf", dpi=300)


plt.show()
