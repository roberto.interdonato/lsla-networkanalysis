import os
import pandas as pd
from LMI_net import getParentVals
import numpy as np
import networkx as nx
import collections



def mines_mlnet(df_deals,df_resources):

    self_investors = set()

    print_nets = True

    layers_minerals=["Gold","Copper","Ironore","Coal","Bauxite","Cobalt","Diamonds","Nickel","Hydrocarbons (e.g. crude oil)","Silver","Zinc","Uranium","Ilmenite","Molybdenum","Lead","Manganese","Titanium","Rutile"]

    layer_ids = {}
    layer_ids_inv = {}

    countries_to_ids = {}
    ids_to_countries = {}

    ml_graph = {}
    id_min = 0

    for min in layers_minerals:
        ml_graph[id_min]=nx.DiGraph()
        layer_ids[id_min]=min
        layer_ids_inv[min]=id_min
        id_min+=1
    # Adding "Other" Layer for small represented resources
    ml_graph[id_min]=nx.DiGraph()
    layer_ids[id_min]="Other"
    layer_ids_inv["Other"]=id_min

    ids = 0
    for index,row in df_deals.iterrows():
        if not pd.isnull(row["Top parent companies"]):
            parent_vals = getParentVals(row["Top parent companies"])
            #print(companies)
            if index in df_resources.index:
                resources_row = df_resources.loc[index]
                resources = []
                r1=resources_row["Resources area 1"]
                r2=resources_row["Resources area 2"]
                r3=resources_row["Resources area 3"]
                r4=resources_row["Resources area 4"]
                if not pd.isnull(r1):
                    resources.append(r1)
                if not pd.isnull(r2):
                    resources.append(r2)
                if not pd.isnull(r3):
                    resources.append(r3)
                if not pd.isnull(r4):
                    resources.append(r4)
            else:
                print("Missing Index: ",index)

            ids_parents = set()
            for pc in parent_vals:
                if len(pc)==3:
                    parent_country = pc[-1].strip().replace('"','')
                    if parent_country != '':  #skyppo country vuoti
                        if parent_country not in countries_to_ids:
                            countries_to_ids[parent_country]=ids
                            ids_to_countries[ids]=parent_country
                            ids+=1
                        ids_parents.add(countries_to_ids[parent_country])
                    else:
                        print("Empty Parent Country: ",pc)
            target_c = row["Location 1: Target country"].strip().replace('"','')
            if target_c not in countries_to_ids:
                countries_to_ids[target_c]=ids
                ids_to_countries[ids] = target_c
                ids+=1
            for pc in ids_parents:
                u = int(pc)
                v = int(countries_to_ids[target_c])
                if u!=v :
                    for r in resources:
                        w = 1
                        if r in layers_minerals:
                            layer = layer_ids_inv[r]
                        else:
                            layer = layer_ids_inv["Other"]

                        if ml_graph[layer].has_edge(u, v):
                            w+=ml_graph[layer][u][v]['weight']
                            ml_graph[layer][u][v]['weight'] = w
                        else:
                            ml_graph[layer].add_edge(u, v, weight=w)
                else :
                    self_investors.add(u)

    if print_nets:
        fout = open("mines_mlnet.ncol",'w')
        fout_dict = open("ids_to_countries_mines_mlnet.csv",'w')
        fout_layer_dict =  open("ids_to_layers_mines_mlnet.csv",'w')

        for id in ids_to_countries:
            fout_dict.write("%s;%s\n" % (id,ids_to_countries[id]))
        for idl in layer_ids:
            fout_layer_dict.write("%s;%s\n" % (idl,layer_ids[idl]))


        for layer in  ml_graph.keys():
            print("Layer %s" % layer_ids[layer])
            G = ml_graph[layer]
            for e in G.edges:
                    fout.write("%d;%d;%d;%d\n" % (e[0],e[1],G[e[0]][e[1]]['weight'],layer))

            print("#Edges",G.size())
            print("#Nodes",G.order(),"\n")

        fout.close()
        fout_dict.close()
        fout_layer_dict.close()

        fsi = open("self_investors.txt",'w')
        for si in self_investors:
            fsi.write("%d\n" % int(si))
        fsi.close()

def mines_mlnet_2023():


    #layers_minerals=["Gold","Copper","Iron","Diamonds","Nickel","Coal","Uranium","Silver","Molybdenum","Lead","Cobalt","Bauxite","Manganese","Zinc"]

    layers_minerals=["Gold","Copper","Iron","Diamonds","Nickel","Coal","Silver","Lead","Zinc","Cobalt","Bauxite"]

    self_investors = set()
    print_nets = True

    os.chdir("/Users/interdonato/Documents/Land Matrix/Exports/27.02.2023.Mines/export/")
    deals_path = "deals.csv"
    investors_path = "investors.csv"

    df_inv = pd.read_csv(investors_path, index_col=0, sep=';')
    df_deals = pd.read_csv(deals_path, sep=';', low_memory=False)

    layer_ids = {}
    layer_ids_inv = {}
    countries_to_ids = {}
    ids_to_countries = {}
    ml_graph = {}
    id_min = 0

    all_resources= set()
    res_col = "Mineral resources area/yield/export"
    for index,row in df_deals.iterrows():
        if not pd.isnull(row[res_col]):
            res_list = row[res_col].split('current')[-1].split('#')[-1].split(',')
            #print(res_list)
            for r in res_list:
                all_resources.add(r.strip())

    for min in layers_minerals:
        min = min.strip()
        ml_graph[id_min] = nx.DiGraph()
        layer_ids[id_min] = min
        layer_ids_inv[min] = id_min
        id_min += 1
    #Adding "Other" Layer for small represented resources
    ml_graph[id_min] = nx.DiGraph()
    layer_ids[id_min] = "Other"
    layer_ids_inv["Other"] = id_min

    ids = 0
    for index, row in df_deals.iterrows():
        if not pd.isnull(row["Top parent companies"]):
            if row["Current implementation status"] == "In operation (production)":
                parent_vals = getParentVals(row["Top parent companies"])
                # print(companies)
                resources = None
                if not pd.isnull(row[res_col]):
                    resources = row[res_col].split('current')[-1].split('#')[-1].split(',')
                else:
                    resources=["Other"]

                ids_parents = []
                for pc in parent_vals:
                    if len(pc) > 1:
                        company_id = pc[1]
                        if not pd.isnull(
                                df_inv.loc[int(company_id), "Country of registration/origin"]):  # skyppo country vuoti
                            parent_country = df_inv.loc[int(company_id), "Country of registration/origin"]
                            if parent_country not in countries_to_ids:
                                countries_to_ids[parent_country] = ids
                                ids_to_countries[ids] = parent_country
                                # if ids==65:
                                #    print(pc)
                                ids += 1
                            ids_parents.append(countries_to_ids[parent_country])
                        else:
                            print("Empty Parent Country: ", pc)
                target_c = row["Target country"].strip().replace('"', '')
                if target_c not in countries_to_ids:
                    countries_to_ids[target_c] = ids
                    ids_to_countries[ids] = target_c
                    ids += 1
                for pc in ids_parents:
                    u = int(pc)
                    v = int(countries_to_ids[target_c])
                    if u != v:
                        for r in resources:
                            r = r.strip()
                            w = 1

                            #layer = layer_ids_inv[r]

                            #print("Other layers disabled. Please de-comment if needed.")

                            if r in layers_minerals:
                                layer = layer_ids_inv[r]
                            else:
                                layer = layer_ids_inv["Other"]


                            if ml_graph[layer].has_edge(u, v):
                                w += ml_graph[layer][u][v]['weight']
                                ml_graph[layer][u][v]['weight'] = w
                            else:
                                ml_graph[layer].add_edge(u, v, weight=w)
                    else:
                        self_investors.add(u)

    if print_nets:
        fout = open("mines_mlnet.ncol_12l", 'w')
        fout_dict = open("ids_to_countries_mines_mlnet_12l.csv", 'w')
        fout_layer_dict = open("ids_to_layers_mines_mlnet_12l.csv", 'w')

        for id in ids_to_countries:
            fout_dict.write("%s;%s\n" % (id, ids_to_countries[id]))
        for idl in layer_ids:
            fout_layer_dict.write("%s;%s\n" % (idl, layer_ids[idl]))

        for layer in ml_graph.keys():
            print("Layer %s" % layer_ids[layer])
            G = ml_graph[layer]
            for e in G.edges:
                fout.write("%d;%d;%d;%d\n" % (e[0], e[1], G[e[0]][e[1]]['weight'], layer))

            print("#Edges", G.size())
            print("#Nodes", G.order(), "\n")

        fout.close()
        fout_dict.close()
        fout_layer_dict.close()

        fsi = open("self_investors_fromML_12l.txt", 'w')
        for si in self_investors:
            fsi.write("%d\n" % int(si))
        fsi.close()


def stats_deals_countries_perLayer():


    #layers_minerals=["Gold","Copper","Iron","Diamonds","Nickel","Coal","Uranium","Silver","Molybdenum","Lead","Cobalt","Bauxite","Manganese","Zinc"]

    layers_minerals=["Gold","Copper","Iron","Diamonds","Nickel","Coal","Silver","Lead","Zinc"]




    self_investors = set()
    print_nets = True

    os.chdir("/Users/interdonato/Documents/Land Matrix/Exports/27.02.2023.Mines/export/")
    deals_path = "deals.csv"

    df_deals = pd.read_csv(deals_path, sep=';', low_memory=False)


    deals_dict = {}
    deals_dict["Other"] = 0
    country_dict = {}
    res_col = "Mineral resources area/yield/export"
    for index, row in df_deals.iterrows():
        if not pd.isnull(row["Top parent companies"]):
            if row["Current implementation status"] == "In operation (production)":

                resources = None
                if not pd.isnull(row[res_col]):
                    resources = row[res_col].split('current')[-1].split('#')[-1].split(',')
                else:
                    resources=["Other"]
                target_c = row["Target country"].strip().replace('"', '')
                for r in resources:
                    r = r.strip()



                    if r in layers_minerals:

                        if r not in deals_dict:
                            deals_dict[r] = 0
                            country_dict[r] = {}


                        deals_dict[r]+=1
                        if target_c not in country_dict[r]:
                            country_dict[r][target_c]=0
                        country_dict[r][target_c]+=1
                    else:
                        deals_dict["Other"] += 1
                        if "Other" not in  country_dict:
                            country_dict["Other"]={}
                        if target_c not in country_dict["Other"]:
                            country_dict["Other"][target_c] = 0
                        country_dict["Other"][target_c]+=1


    print(deals_dict)

    for c in country_dict:
        d = country_dict[c]
        print("========================================")
        print(c)
        print("========================================")
        for w in sorted(d, key=d.get, reverse=True):
            print(w, d[w])
        print("========================================")

if __name__=='__main__':
    mines_mlnet_2023()
    #stats_deals_countries_perLayer()
