import networkx as nx
#from mpl_toolkits.basemap import Basemap as Basemap
import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.io.shapereader as shpreader
import itertools
import numpy as np
from colour import Color
from collections import OrderedDict
import matplotlib as mpl
#shapefile = "D:\\Mes Donnees\\Land Matrix\\Shapefile\\"
from mpl_toolkits.axes_grid1 import make_axes_locatable
from geopandas import GeoDataFrame
from descartes import PolygonPatch
import os

dataset_name = 'mines'
#dataset_name = "agriculture+biofuel"
#dataset_name = "global"

os.chdir("/Users/interdonato/Documents/Land Matrix/Exports/27.02.2023.Mines/nets/")
path_iso = "ISO country.xlsx"
path_d = "ids_to_countries_mines_27022023_inOperation.csv"
path = "net_mines_27022023_inOperation.ncol"

#path = ".\\data_net\\net_%s_surf_inOperation.ncol" % dataset_name
#path_d = ".\\data_net\\ids_to_countries_%s_inOperation.csv" % dataset_name

graph = nx.read_edgelist(path, delimiter=';', create_using=nx.DiGraph(), nodetype=int, data=(('weight', int),))



path_agriland = ".\\data_net\\AgLand.xlsx"

names_dict = {}
coord = {}
lat = []
long = []


fc = open("countries_to_coord_allTransnational.csv", 'r')
for line in fc:
    vals=line.split(';')
    coord[vals[0]] = (float(vals[1]), float(vals[2].strip()))

inv_names_dict = {}

fd = open(path_d,'r')
for line in fd:
    vals=line.split(';')
    country = vals[1].strip()
    names_dict[int(vals[0])]=country
    inv_names_dict[country]=int(vals[0])
    #location = geolocator.geocode(vals[1])
    lat.append(coord[country][0])
    long.append(coord[country][1])
    #fc.write("%s;%s;%s\n"   %  (vals[1].strip(),str(coord[vals[1]][0]),str(coord[vals[1]][1])))
print("Names and coordinates successfully loaded.")

fc.close()

pos = {}
for id in names_dict:
        pos[int(id)]=(coord[names_dict[id]][1],coord[names_dict[id]][0])


iso =  pd.read_excel(path_iso, index_col=0, sep=';')

iso_dict = {}
inv_iso_dict= {}
for id in names_dict:
    iso_dict[id]=iso.loc[names_dict[id]]['Code ISO']
    inv_iso_dict[iso.loc[names_dict[id]]['Code ISO']]=id

size_deg_dict= dict(graph.in_degree(weight='weight'))

min_d = np.min(list(size_deg_dict.values()))
for x in size_deg_dict:
    if size_deg_dict[x]==0:
        size_deg_dict[x]=min_d

"""
fom = open("SizesInOut_%s.csv" % dataset_name,'w')
for n in size_deg_dict:
    fom.write("%s;%f\n" % (names_dict[n],size_deg_dict[n]))
fom.close()
"""

df_agri = pd.read_excel(path_agriland,index_col=1)

fo_ag = open("Ratio_AgLand_%s.csv" % dataset_name,'w')
fo_tot = open("Ratio_TotLand_%s.csv" % dataset_name,'w')
for n in size_deg_dict:
    iso=iso_dict[n]
    if iso in df_agri.index:
        agriland = float(df_agri.loc[iso]["Ag Land 2016 (Ha)"])
        totland = float(df_agri.loc[iso]["Land area (Ha) 2018"])
        if agriland!=0 and totland!=0:
            fo_ag.write("%s;%f\n" % (names_dict[n],float(size_deg_dict[n])/agriland))
            fo_tot.write("%s;%f\n" % (names_dict[n], float(size_deg_dict[n]) / totland))
fo_ag.close()
fo_tot.close()