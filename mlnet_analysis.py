import os

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
import glob
from os.path import basename
import math

def readNet(input_path,separator=';'):
    layer_graphs = {}
    weights = {}
    node_layers = {}

    f = open(input_path, 'r')
    for line in f:
        vals = line.split(separator)

        u = int(vals[0].strip())
        v = int(vals[1].strip())
        w = int(vals[2].strip())
        l = int(vals[3].strip())

        if l not in layer_graphs:
            layer_graphs[l] = nx.DiGraph()
            weights[l]=[]

        layer_graphs[l].add_edge(u, v)

        weights[l].append(w)

        if u not in node_layers:
            node_layers[u] = set()
        if v not in node_layers:
            node_layers[v] = set()
        node_layers[u].add(l)
        node_layers[v].add(l)
    return layer_graphs,node_layers,weights

def readNet_withWeights(input_path,separator=';'):
    layer_graphs = {}

    f = open(input_path, 'r')
    for line in f:
        vals = line.split(separator)

        u = int(vals[0].strip())
        v = int(vals[1].strip())
        w = int(vals[2].strip())
        l = int(vals[3].strip())

        if l not in layer_graphs:
            layer_graphs[l] = nx.DiGraph()


        if layer_graphs[l].has_edge(u, v):
            w+=layer_graphs[l][u][v]['weight']
            layer_graphs[l][u][v]['weight'] = w
        else:
            layer_graphs[l].add_edge(u, v, weight=w)



    return layer_graphs

def structuralStats(graph,nlay):

    n =  len(nlay)
    print("#nodes: ",n)
    ed = 0
    den = 0.0

    for l in graph:
        ed+=len(graph[l].edges())
        nl = len(graph[l].nodes())
        den+= nl*(nl-1)
        #print("CC: ",l,nx.number_connected_components(graph[l]))
        #ccs = nx.connected_components(graph[l])
        #lens = []
        #for c in ccs:
        #    lens.append(len(c))
        #print("CC lengths: ", sorted(lens))

    print("#edges: ",ed)
    layers = len(graph)
    print("#layers: ", layers)
    density = (2*ed)/(den)
    print("Density: ", density)
    print("Adeg: ", (ed*2)/n)
    ls = []
    for n in nlay:
        ls.append(len(nlay[n]))
    alayer = np.mean(ls)
    print("ALayer: ",alayer)

def ml_stats(input_path,layer_ids):
    layer_graphs,node_layers,weights = readNet(input_path)

    print("Graph loaded, calculating statistics")

    avg_weights = {}

    for layer in weights:
        avg_weights[layer] = np.mean(weights[layer])

    print("Stats per layer:")
    avgs = []
    for l in avg_weights:
        avgs.append(avg_weights[l])
        print(layer_ids.iloc[l][1],":   avg weight:",avg_weights[l],"#nodes",len(layer_graphs[l].nodes()),"edges",len(layer_graphs[l].edges()))
    print("\nglobal avg intra weight", np.mean(avgs))

    structuralStats(layer_graphs,node_layers)



    ed = 0
    den = 0.0

    node_perl = []

    for l in layer_graphs:
        ed += len(layer_graphs[l].edges())
        nl = len(layer_graphs[l].nodes())
        node_perl.append(nl)
        den += nl * (nl - 1)
        # print("CC: ",l,nx.number_connected_components(graph[l]))
        # ccs = nx.connected_components(graph[l])
        # lens = []
        # for c in ccs:
        #    lens.append(len(c))
        # print("CC lengths: ", sorted(lens))

    print("Average nodes per layer",np.mean(node_perl)," std ",np.std(node_perl))

    print("Detailed stats per layer")
    for l in layer_graphs:
        print("=================================================")
        print("Layer: ",l,layer_ids.iloc[l][1])
        G = layer_graphs[l]
        print("------- Directed ---------")
        graph_stats_directed(G)
        print("------- Undirected ---------")
        G = G.to_undirected()
        graph_stats_undirected(G)

def graph_stats_directed(G):


    #path = ".\\data_net\\net_allTransnational.ncol"
    G = nx.DiGraph(G)
    print("#Edges",G.size())
    print("#Nodes",G.order())
    print("Transitivity",nx.transitivity(G))
    #print("Average CC", nx.average_clustering(G))
    #    print("Connected",nx.is_connected(G))
    #   print("#Connected Components", nx.number_connected_components(G))
    print("Assortativity", nx.degree_assortativity_coefficient(G))

    sum=0
    for t in G.degree:
        sum+=t[1]
    sum/=len(G.degree)
    print("Avg Degree",sum)

    if (nx.is_weakly_connected(G)):
        print("Average Path Length",nx.average_shortest_path_length(G))
    else:
        print("Not weakly connected")


    tot = 0
    all = 0
    length = 0
    for u in G.nodes():
        for v in G.nodes():
            if v != u:
                if nx.has_path(G, u, v):
                    tot += 1
                    length+=nx.shortest_path_length(G,u,v)
                all += 1

    print(tot, all, tot / all, length/tot)

    count = 0

    for e in G.edges:
        if G.has_edge(e[1],e[0]) and e[0]!=e[1]:
            count+=1
    print("Percentage of reciprocal edges",count/len(G.edges))



def graph_stats_undirected(G):

    print("#Edges",G.size())
    print("#Nodes",G.order())
    print("Transitivity",nx.transitivity(G))
    print("Average CC", nx.average_clustering(G))
    print("Connected",nx.is_connected(G))
    print("#Connected Components", nx.number_connected_components(G))
    print("Assortativity", nx.degree_assortativity_coefficient(G))
    sum=0
    for t in G.degree:
        sum+=t[1]
    sum/=len(G.degree)
    print("Avg Degree",sum)

    #print("Average Path Length", nx.average_shortest_path_length(G))
    if (nx.is_connected(G)):
        print("Average Path Length", nx.average_shortest_path_length(G))
    else:
        print("Not weakly connected")
        for c in nx.connected_components(G):
            g =  G.subgraph(c)
            print("Average Path Length cc", nx.average_shortest_path_length(g),g.nodes())
    tot = 0
    all = 0
    for u in G.nodes():
        for v in G.nodes():
            if v!=u:
                if nx.has_path(G,u,v):
                    tot+=1
                all+=1

    print(tot,all,tot/all)


def ncol_to_infomap(input,output):

    #  -i multilayer --ftree --clu -f directed -d


    layer_graphs,node_layers,weights = readNet(input)
    ids = pd.read_csv("ids_to_countries_mines_mlnet.csv",sep=';',header=None)

    f_in = open(input,'r')
    f_out = open(output,'w')
    f_out.write("*Vertices %d\n" % len(node_layers))
    for index,row in ids.iterrows():
        f_out.write("%s \"%s\"\n" % (str(index),row[1]))
    #layer_id node_id node_id weight
    f_out.write("*Multilayer\n")
    f_out.write("*Intra\n")
    for line in f_in:
        vals = line.split(';')
        u = vals[0]
        v = vals [1]
        w = vals[2]
        l = vals[3]
        outline = "%s %s %s %s\n" % (l.strip(),u,v,w)
        f_out.write(outline)
    f_out.close()


def read_infomap_communities():
    ids = pd.read_csv("/Users/interdonato/Documents/Land Matrix/Exports/27.02.2023.Mines/nets/ids_to_countries_mines_mlnet_10l.csv",sep=';',header=None)
    infomap = open("/Users/interdonato/Documents/Land Matrix/Exports/27.02.2023.Mines/communities/network_10l/network.clu",'r')
    communities = {}

    for line in infomap:
        if line[0]!='#':
            vals=line.split(' ')
            com = int(vals[1])
            id = int(vals[0])
            if com not in communities:
                communities[com]=set()
            communities[com].add(ids.iloc[id][1])
    for idc in communities:
        #print(idc,communities[idc])
        print(idc,";".join(c for c in list(communities[idc])),sep=';')

def flatten_mlnet(netfile):

    G = nx.DiGraph()
    fin = open(netfile,'r')
    for line in fin:
        vals = line.split(';')
        u = int(vals[0])
        v = int(vals[1])
        w = int(vals[2])
        if G.has_edge(u,v):
            w+=G[u][v]['weight']
            G[u][v]['weight'] = w
        else:
            G.add_edge(u, v, weight=w)
    fout = open("mines_mlnet_flattened_10l.ncol",'w')
    for e in G.edges:
        fout.write("%d;%d;%d\n" % (e[0],e[1],G[e[0]][e[1]]['weight']))
    fout.close()
    print("#Edges",G.size())
    print("#Nodes",G.order(),"\n")

def getID(country,id_df):
    for index,row in id_df.iterrows():
        if(row[1]==country):
            return index

def country_stats(country):
    netfile = "mines_mlnet.ncol"
    layer_ids = pd.read_csv("ids_to_layers_mines_mlnet.csv",sep=';',header=None)
    #ml_stats(netfile,layer_ids)
    country_ids = pd.read_csv("ids_to_countries_mines_mlnet_noSpecialChars.csv",sep=';',header=None)
    curr_id = getID(country,country_ids)

    layer_graphs,node_layers,weights = readNet(netfile)

    sinks = []
    sources = []
    for g in layer_graphs:
        graph = layer_graphs[g]
        if curr_id in graph.nodes():
            indeg = graph.in_degree(curr_id)
            outdeg =  graph.out_degree(curr_id)
            print(layer_ids.iloc[g][1],indeg,outdeg)
        else :
            print(layer_ids.iloc[g][1],"%s NOT IN THIS LAYER" % country)



def ml_layers_print(input_path,layer_ids):
    layer_graphs = readNet_withWeights(input_path)

    path_d = "ids_to_countries_mines_mlnet_12l.csv"
    path_iso = "ISO country.xlsx"
    iso = pd.read_excel(path_iso, index_col=0)
    f = open(path_d,'r')
    labels = {}
    for line in f:
        vals = line.split(';')
        labels[int(vals[0])]=iso.loc[vals[1].strip(),"Code ISO"]

    print(labels)
    print("Graph loaded.")

    degs = ["in","out","btw"]

    d = degs[0]
    for l in layer_graphs:

        print("=================================================")
        print("Layer: ",l,layer_ids.iloc[l][1])
        #if layer_ids.iloc[l][1]=="Cobalt" or layer_ids.iloc[l][1]=="Bauxite":
        G = layer_graphs[l]

        pos = nx.spring_layout(G, seed=7, k = ((1/len(G.nodes))*5) )  # positions for all nodes - seed for reproducibility

        if d=="out":
            size_deg_dict = dict(G.out_degree(weight='weight'))
        elif d=='in':
            size_deg_dict = dict(G.in_degree(weight='weight'))
        elif d=='btw':
            Gno = G.to_undirected()
            size_deg_dict = dict(nx.betweenness_centrality(Gno))
        print(size_deg_dict)
        nx.draw_networkx_nodes(G=G,pos=pos,node_size=[(v * 300)+1 for v in size_deg_dict.values()])
        these_labels = {}
        for n in G.nodes:
            these_labels[n]=labels[n]
        nx.draw_networkx_edges(G=G, pos=pos, width=1, edge_color="darkgreen", alpha=0.2, style="solid",arrows=True, connectionstyle='arc3,rad=0.2')
        nx.draw_networkx_labels(G,pos,labels=these_labels,font_size=8)
        #nx.draw_networkx_labels(G, pos)
        #plt.show()
        plt.savefig("layer_%s_%s.pdf" % (layer_ids.iloc[l][1],d), format="pdf", dpi=300)
        plt.clf()



if __name__=='__main__':



    os.chdir("/Users/interdonato/Documents/Land Matrix/Exports/27.02.2023.Mines/nets/")
    #country_stats("China")
    #exit()
    #flatten_mlnet("mines_mlnet_10l.ncol")

    netfile = "mines_mlnet_12l.ncol"
    layer_ids = pd.read_csv("ids_to_layers_mines_mlnet_12l.csv",sep=';',header=None)

    #ncol_to_infomap(netfile,"mines_mlnet_wnames_10l.net")
    #read_infomap_communities()

    ml_layers_print(netfile, layer_ids)

    exit()
    """"
    files = glob.glob("Indegree_stats\\*.csv")
    for file in files:
        df = pd.read_csv(file,sep=';',header=None, encoding = "ISO-8859-1")
        df.sort_values(2,axis=0,inplace=True,ascending=False)
        row = "%s " % basename(file)
        for i in range(5):
            row+= "%s (%s) " % (df.iloc[i][1],df.iloc[i][2])
        print(row)
    """

    """"
    netfile = "mines_mlnet.ncol"
    layer_graphs = readNet_withWeights(netfile)

    #for w in weights:
    #    print(w,np.sum(weights[w]))

    path_d = "ids_to_countries_mines_mlnet.csv"
    names_dict = {}

    fd = open(path_d,'r')
    for line in fd:
        vals=line.split(';')
        country = vals[1].strip()
        names_dict[int(vals[0])]=country


    for g_id in layer_graphs:
        size_deg_dict = dict(layer_graphs[g_id].in_degree())
        fom = open("SizesIn_Layer_%d.csv" % g_id,'w')
        for n in size_deg_dict:
            fom.write("%d;%s;%d\n" % (n,names_dict[n],size_deg_dict[n]))
        fom.close()
    """

    """
    #read_infomap_communities()



    layer_ids = pd.read_csv("ids_to_layers_mines_mlnet.csv",sep=';',header=None)
    #ml_stats(netfile,layer_ids)

    layer_graphs,node_layers,weights = readNet(netfile)

    sinks = []
    sources = []
    for g in layer_graphs:
        sink = 0
        source = 0
        graph = layer_graphs[g]
        indict = dict(graph.in_degree())
        outdict = dict(graph.out_degree())
        for i in indict:
            if indict[i]==0:
                source+=1
        for i in outdict:
            if outdict[i]==0:
                sink+=1
        sinks.append(sink)
        sources.append(source)
        print(layer_ids.iloc[g][1],len(graph.nodes()),sink,source,(len(graph.nodes())-sink-source))

    print("Sinks: ",np.mean(sinks),np.std(sinks))
    print("Sources: ",np.mean(sources),np.std(sources))

    ml_stats(netfile,layer_ids)
    """
