import os
import pandas as pd
from LMI_net import getParentVals
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.cluster import OPTICS
from sklearn.cluster import MeanShift
import networkx as nx

os.chdir("/Users/interdonato/Documents/Land Matrix/Exports/27.02.2023.Mines/export/")
deals_file = "deals.csv"
resources_file = "Resources_new.xlsx"
resource_list = "ListResources.csv"

def list_all_resources(df_deals):
    res_col = "Mineral resources area/yield/export"
    res_dict = {}
    for index,row in df_deals.iterrows():
        deal_id = index
        if not pd.isnull(row[res_col]):
            res_list = row[res_col].split('current')[-1].split('#')[-1].split(',')
            #print(res_list)
            for r in res_list:
                r = r.strip()
                if r not in res_dict.keys():
                    res_dict[r]=0
                res_dict[r]+=1
    for r in res_dict:
        print(r,res_dict[r],sep=';')

def list_resources():
    all_resources = set()
    for index,row in df_deals.iterrows():
        if not pd.isnull(row["Top parent companies"]):
            companies = getParentVals(row["Top parent companies"])
            #print(companies)
            if index in df_resources.index:
                resources_row = df_resources.loc[index]
                resources = []
                r1=resources_row["Resources area 1"]
                r2=resources_row["Resources area 2"]
                r3=resources_row["Resources area 3"]
                r4=resources_row["Resources area 4"]
                if not pd.isnull(r1):
                    resources.append(r1)
                if not pd.isnull(r2):
                    resources.append(r2)
                if not pd.isnull(r3):
                    resources.append(r3)
                if not pd.isnull(r4):
                    resources.append(r4)
                for r in resources:
                    all_resources.add(r)
            else:
                print("Missing Index: ",index)

    for r in all_resources:
        print(r)

def get_clusters(labels,cids):
    clust_dict = {}
    idx = 0
    for c in labels:
        if c not in clust_dict:
            clust_dict[c]=[]
        clust_dict[c].append(cids[idx])
        idx+=1
    return clust_dict

def print_clusters(clust_dict):
    for c in clust_dict.keys():
        print(c,clust_dict[c],"\n")



def clustering():
    bin_data = {}
    sum_data = {}
    for index,row in df_deals.iterrows():
        if not pd.isnull(row["Top parent companies"]):
            resources_array = np.zeros((len(df_resource_list)),dtype=np.int8)
            parents = getParentVals(row["Top parent companies"])
            #print(companies)
            if index in df_resources.index:
                resources_row = df_resources.loc[index]
                resources = []
                r1=resources_row["Resources area 1"]
                r2=resources_row["Resources area 2"]
                r3=resources_row["Resources area 3"]
                r4=resources_row["Resources area 4"]
                if not pd.isnull(r1):
                    resources.append(r1)
                if not pd.isnull(r2):
                    resources.append(r2)
                if not pd.isnull(r3):
                    resources.append(r3)
                if not pd.isnull(r4):
                    resources.append(r4)
                for r in resources:
                    resources_array[res_ids[r]]=1
                #print(resources_array)

                countries = []
                for par in parents:
                    if len(par)==3:
                        countries.append(par[-1])

                for c in countries:
                    if c not in bin_data:
                        bin_data[c]=np.zeros((len(df_resource_list)),dtype=np.int8)
                        sum_data[c]=np.zeros((len(df_resource_list)),dtype=np.int8)
                    bin_data[c]=np.bitwise_or(bin_data[c],resources_array)
                    sum_data[c]+=resources_array
            else:
                print("Missing Index: ",index)

    bin_mat = np.array(list(bin_data.values()))
    sum_mat = np.array(list(sum_data.values()))
    #print(bin_mat.shape)
    #print(bin_mat)
    c_ids = {}
    inv_c_ids = {}
    id = 0
    for x in sum_data.keys():
        c_ids[x]=id
        inv_c_ids[id]=x
        id+=1

    #df_bin = pd.DataFrame(bin_mat,index=c_ids,columns=res_ids.keys())
    #df_sum = pd.DataFrame(sum_mat,index=c_ids,columns=res_ids.keys())

    #df_bin.to_csv("binary_resource_matrix.csv",sep=';')
    #df_sum.to_csv("summary_resource_matrix.csv",sep=';')

    """
    clustering = DBSCAN(eps=3, min_samples=2).fit(bin_mat)
    clustering2 = OPTICS(min_samples=2).fit(bin_mat)
    clustering3 = DBSCAN(eps=3, min_samples=2).fit(sum_mat)
    clustering4 = OPTICS(min_samples=2).fit(sum_mat)
    """

    clustering5 = MeanShift(bandwidth=2).fit(bin_mat)
    clustering6 = MeanShift(bandwidth=2).fit(sum_mat)

    """
    print("\n\nDB Scan - Binary\n\n")
    print_clusters(get_clusters(clustering.labels_,inv_c_ids))
    print("\n\nOptics - Binary\n\n")
    print_clusters(get_clusters(clustering2.labels_,inv_c_ids))
    print("\n\nDB Scan - Sum\n\n")
    print_clusters(get_clusters(clustering3.labels_,inv_c_ids))
    print("\n\nOptics - Sum\n\n")
    print_clusters(get_clusters(clustering4.labels_,inv_c_ids))
    """


def mines_mlnet(df_deals,df_resources):

    self_investors = set()

    print_nets = True

    layers_minerals=["Gold","Copper","Ironore","Coal","Bauxite","Cobalt","Diamonds","Nickel","Hydrocarbons (e.g. crude oil)","Silver","Zinc","Uranium","Ilmenite","Molybdenum","Lead","Manganese","Titanium","Rutile"]

    layer_ids = {}
    layer_ids_inv = {}

    countries_to_ids = {}
    ids_to_countries = {}

    ml_graph = {}
    id_min = 0

    for min in layers_minerals:
        ml_graph[id_min]=nx.DiGraph()
        layer_ids[id_min]=min
        layer_ids_inv[min]=id_min
        id_min+=1
    # Adding "Other" Layer for small represented resources
    ml_graph[id_min]=nx.DiGraph()
    layer_ids[id_min]="Other"
    layer_ids_inv["Other"]=id_min

    ids = 0
    for index,row in df_deals.iterrows():
        if not pd.isnull(row["Top parent companies"]):
            parent_vals = getParentVals(row["Top parent companies"])
            #print(companies)
            if index in df_resources.index:
                resources_row = df_resources.loc[index]
                resources = []
                r1=resources_row["Resources area 1"]
                r2=resources_row["Resources area 2"]
                r3=resources_row["Resources area 3"]
                r4=resources_row["Resources area 4"]
                if not pd.isnull(r1):
                    resources.append(r1)
                if not pd.isnull(r2):
                    resources.append(r2)
                if not pd.isnull(r3):
                    resources.append(r3)
                if not pd.isnull(r4):
                    resources.append(r4)
            else:
                print("Missing Index: ",index)

            ids_parents = set()
            for pc in parent_vals:
                if len(pc)==3:
                    parent_country = pc[-1].strip().replace('"','')
                    if parent_country != '':  #skyppo country vuoti
                        if parent_country not in countries_to_ids:
                            countries_to_ids[parent_country]=ids
                            ids_to_countries[ids]=parent_country
                            ids+=1
                        ids_parents.add(countries_to_ids[parent_country])
                    else:
                        print("Empty Parent Country: ",pc)
            target_c = row["Location 1: Target country"].strip().replace('"','')
            if target_c not in countries_to_ids:
                countries_to_ids[target_c]=ids
                ids_to_countries[ids] = target_c
                ids+=1
            for pc in ids_parents:
                u = int(pc)
                v = int(countries_to_ids[target_c])
                if u!=v :
                    for r in resources:
                        w = 1
                        if r in layers_minerals:
                            layer = layer_ids_inv[r]
                        else:
                            layer = layer_ids_inv["Other"]

                        if ml_graph[layer].has_edge(u, v):
                            w+=ml_graph[layer][u][v]['weight']
                            ml_graph[layer][u][v]['weight'] = w
                        else:
                            ml_graph[layer].add_edge(u, v, weight=w)
                else :
                    self_investors.add(u)

    if print_nets:
        fout = open("mines_mlnet.ncol",'w')
        fout_dict = open("ids_to_countries_mines_mlnet.csv",'w')
        fout_layer_dict =  open("ids_to_layers_mines_mlnet.csv",'w')

        for id in ids_to_countries:
            fout_dict.write("%s;%s\n" % (id,ids_to_countries[id]))
        for idl in layer_ids:
            fout_layer_dict.write("%s;%s\n" % (idl,layer_ids[idl]))


        for layer in  ml_graph.keys():
            print("Layer %s" % layer_ids[layer])
            G = ml_graph[layer]
            for e in G.edges:
                    fout.write("%d;%d;%d;%d\n" % (e[0],e[1],G[e[0]][e[1]]['weight'],layer))

            print("#Edges",G.size())
            print("#Nodes",G.order(),"\n")

        fout.close()
        fout_dict.close()
        fout_layer_dict.close()

        fsi = open("self_investors.txt",'w')
        for si in self_investors:
            fsi.write("%d\n" % int(si))
        fsi.close()


if __name__=='__main__':
    df_deals = pd.read_csv(deals_file,sep=';',index_col=0)
    list_all_resources(df_deals)
    exit()
    df_resources = pd.read_excel(resources_file,index_col=0)
    df_resource_list = pd.read_csv(resource_list,sep=';',index_col=0)

    res_ids = {}

    for index,row in df_resource_list.iterrows():
        res_ids[row["resource"]]=int(index)

    mines_mlnet(df_deals,df_resources)