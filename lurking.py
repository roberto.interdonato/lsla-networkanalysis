import os
import pandas as pd
import glob
from os.path import basename
import networkx as nx
# the original code of lurkerRank can be found at : https://github.com/andreatagarelli/lurkerrank
import lurkerRank as lr



def ids_to_names():
    os.chdir(".\\_LURKER\\")
    names_df = pd.read_csv(".\\net\\ids_to_countries_global_07072021_inOperation.csv",sep=';',encoding="latin1")

    ranking_files = glob.glob(".\\rankings\\*.txt")
    for rf in ranking_files:
        print("Processing :",rf)
        foname = basename(rf).split('.')[0]+"_CountryNames.csv"
        fo = open(foname,'w')
        df = pd.read_csv(rf,sep=';')
        for index,row in df.iterrows():
            country = names_df.loc[index][1]
            fo.write("%s;%f\n" % (country,row[1]))
        fo.close()

# Excludes source and sink nodes
def ids_to_names_SSfilter():
    os.chdir(".\\_LURKER\\")

    dataset = "biofuels"

    names_df = pd.read_csv(".\\net_%s\\ids_to_countries_%s_17032022_inOperation.csv" % (dataset,dataset),sep=';',encoding="latin1",index_col=0,header=None)
    netpath = ".\\net_%s\\net_%s_17032022_inOperation_LR.ncol"  % (dataset,dataset)
    G = nx.read_edgelist(netpath,delimiter=';',create_using=nx.DiGraph(), nodetype=int, data=(('weight',float),))
    ranking_files = glob.glob(".\\net_%s\\rankings\\*.txt" % dataset)
    for rf in ranking_files:
        print("Processing :",rf)
        foname = basename(rf).split('.')[0]+"_CountryNames_Filtered.csv"
        fo = open(foname,'w')
        df = pd.read_csv(rf,sep=';',index_col=0,header=None)
        for index,row in df.iterrows():
            country = names_df.iloc[index][1]
            #print(index,G.out_degree(index),G.in_degree(index))
            if G.out_degree(index)==0 or G.in_degree(index)==0:
                #print("Sink or source : ",index)
                if country=="Brazil":
                    print(index,G.out_degree(index),G.in_degree(index))
            else :
                fo.write("%s;%f\n" % (country,row[1]))
        fo.close()

def computeLR():
    os.chdir(".\\_LURKER\\")
    dataset = "biofuels"
    lrs = ['LRin', 'LRout', 'LRinout']
    for algo in lrs:
        lr.compute_on_dir("net_%s\\" % dataset,"net_%s\\rankings\\" % dataset,algo)


if __name__=="__main__":
    ids_to_names_SSfilter()
    #computeLR()
