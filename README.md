# LSLA-NetworkAnalysis

A python framework to analyze data about large scale land acquisition extracted from the Land Matrix database from a network analysis perspective. 


# LM_RawData

The *LM_RawData* folder contains snapshots of the Land Matrix Initiative database, downloaded from its official website https://landmatrix.org on **16 January 2020**. 

More specifically: 

-  The "global" folder contains a snapshot which includes all transnational deals
-  The "agriculture+biofuel" folder contains a snapshot which includes all the deals having an Intention of investment in one of the following categories: Biofuels, Fodder, Food crops, Agriculture unspecified, Livestock, Non-food agricultural commodities.
-  The "mines" folder contains a snapshot which includes all the deals having "mining" as Intention of investment

All the original data is made available in open access by the Land Matrix Initiative. 
The snapshots have been exported from the https://landmatrix.org/data/ website by using the dedicated filters on the Land Matrix database attributes. 

# LM_RawData_Anomalies

The *LM_RawData_Anomalies* folder contains snapshots of the Land Matrix Initiative database, downloaded from its official website https://landmatrix.org on **17 March 2022**. 

More specifically, it contains a snapshot including transnational deals having an "In Operation" status. 

All the original data is made available in open access by the Land Matrix Initiative. 
The snapshot has been exported from the https://landmatrix.org/data/ website by using the dedicated filters on the Land Matrix database attributes. 


# data_net

The *data\_net* folder contains the network files (in the ncol/edgelist format) corresponding to different land trade networks, generated using the **LMI\_net.py** script.
The folder also containes additional input files for specific analyses (coordinates of the countries, iso codes, table of development indexes, etc.). 

# data_net_anomalies

The *data_net_anomalies* folder contains the network files (in the ncol/edgelist format) corresponding to different land trade networks, generated using the **LMI\_net.py** script, to be used with the **lurking.py** script for identifying and ranking anomalous behaviors in the land trade networks. 
The folder also containes additional input files for specific analyses (coordinates of the countries, iso codes, table of development indexes, etc.). 

# Geonet Maps

The *geonet\_gradient.py* and *geonet\_gradient\_selected.py* scripts can be used to generate world maps based on the cartopy library. 
With *geonet\_gradient\_selected.py* it is possible to select a subset of countries to show, by listing their iso codes in the "selected_iso" variable.

Countries are colored based on a score proportional to the weighted in/out-degree ratio of each country (LSLA-Score), i.e., the ratio between acquired and sold land for each country.
Using *geonet\_gradient.py* is also possible to export (in the csv format) information about the indegree, outdegree and LSLA-score of each country.

The *geonet\_3roles.py* and *geonet\_3roles\_selected.py*  work under the same principles, but displaying the investor/target/double role of the countries. 

# Lurking

The **lurking.py** script can be used for identifying and ranking anomalous behaviors in the land trade networks. Network files ending with the "_LR" suffix should be used (topology where an edge u->v goes from the target to the investor country, following the flow of the acquired land).
Note that, to work properly, it needs the original code from LurkerRank, that can accessed here: 

https://github.com/andreatagarelli/lurkerrank
